# Flux GitLab Syncer

This project creates a small app that watches for changes in Flux v2 resources and syncs them to GitLab. 
This helps facilitate additional automation and reduce the amount of manual configuration to get GitOps up and running. 
Specifically, it watches the following:

- `GitRepository` objects to add/remove the SSH keys to referenced git repos
- `Receiver` objects to add/remove the configured webhook URLs to the repos

Note that this component currently watches only these resources within a single namespace, as this was built to
run in a multi-tenant platform and automate the setup of the tenant repos.

## Configuration

The following environment variables can be used to configure the syncer.

| Name | Required? | Description | Default Value |
|------|-----------|-------------|---------------|
| `GITLAB_PRIVATE_KEY_FILE` | Yes | File path to a file containing the API key/access token used to authenticate with the GitLab API server. Note this token needs full API access to add/remove keys and webhooks | - |
| `WEBHOOK_BASE_URL` | Yes | The base URL to prepend to the URLs created by the `Receiver` objects. This should be the URL that resolves to the webhook receiver. Example: `https://webhook-receiver.example.com` | - |
| `NAMESPACE` | Yes | The namespace to watch for resources | - |
| `DEPLOY_KEY_IDENTIFIER` | No | The name given to the Deploy Key in GitLab | `flux-gitlab-key-syncer` |
| `GITLAB_HOST` | No | The host of the GitLab server | `https://code.vt.edu` |
| `PORT` | No | The port to run the HTTP server on | `8080` |


## Deployment Example

You can use the following instructions to deploy the syncer.

1. Set an environment variable for the namespace you want to deploy everything into. This is used to support copy/paste of the following commands.

    ```cli
    export NAMESPACE=platform-flux-tenant-config 
    ```

1. In GitLab, create an access token. We recommend creating a bot account that can be added to all of the repos/groups and using a personal access token for this bot.
   Hopefully, GitLab will add support for group-level access tokens. Store this token in a file named `gitlab-token`.
   
1. Create a Kubernetes secret containing the access token.

    ```cli
   kubectl create secret generic gitlab-api-token --from-file=token=./gitlab-token
   ```

1. Deploy the syncer! Note that the configured `WEBHOOK_BASE_URL` points to https://webhook-receiver.example.com, which obviously isn't going to work for you.

    ```cli
   kubectl apply -f ./examples -n $NAMESPACE
   ```

1. After a moment, check the logs for the deployed pod. You should see some startup info letting you know how it's configured.

    ```cli
     kubectl logs flux-gitlab-syncer-d9fd798d-gd9ms
    [INFO]  2021-06-25T16:44:56.143Z Configured GitLab client to use https://code.vt.edu with token Ww****
    [INFO]  2021-06-25T16:44:56.162Z Configuring WebhookSyncer to use base URL of https://webhook-receiver.example.com
    [INFO]  2021-06-25T16:44:26.180Z Web server listening on port 8080
    ```
