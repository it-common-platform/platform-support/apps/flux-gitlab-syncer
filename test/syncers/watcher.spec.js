const Watcher = require("../../src/syncers/watcher");

const GROUP = "test.example.com";
const VERSION = "v1";
const PLURAL = "resources";
const POLL_PERIOD = 100;

let watcher, k8sClient, mockedWatcher;

beforeEach(() => {
  k8sClient = {
    watchCustomResource: jest.fn(),
    listCustomResources: jest.fn(),
  };

  watcher = new Watcher(k8sClient, GROUP, VERSION, PLURAL, POLL_PERIOD);
  mockedWatcher = {
    abort: jest.fn(),
  };
});

afterEach(() => {
  watcher.stop();
});

it("has default poll period of five minutes", () => {
  watcher = new Watcher(k8sClient, GROUP, VERSION, PLURAL);
  expect(watcher.pollPeriod).toBe(5 * 60 * 1000);
});

it("doesn't throw if stopping without starting", () => {
  watcher.stop();
});

it("starts and stops correctly", async () => {
  k8sClient.watchCustomResource.mockResolvedValue(mockedWatcher);
  const callback = jest.fn();

  await watcher.start(callback);
  watcher.stop();

  // Validate the watcher request was aborted/closed correctly
  expect(mockedWatcher.abort).toHaveBeenCalled();
  expect(callback).not.toHaveBeenCalled();

  // Validate the poll was cleared properly and doesn't run
  await wait(POLL_PERIOD * 2);
  expect(callback).not.toHaveBeenCalled();
});

it("triggers a poll and sends all listed resources via the callback", async () => {
  const mockResources = [{ id: 1 }, { id: 2 }];
  const callback = jest.fn();

  k8sClient.watchCustomResource.mockResolvedValue(mockedWatcher);
  k8sClient.listCustomResources.mockResolvedValue(mockResources);

  await watcher.start(callback);

  await wait(POLL_PERIOD * 1.5);
  expect(callback).toHaveBeenCalledTimes(mockResources.length);
  mockResources.forEach((r) => expect(callback).toHaveBeenCalledWith("POLL", r));

  watcher.stop();
});

function wait(millis) {
  return new Promise((acc) => setTimeout(acc, millis));
}
