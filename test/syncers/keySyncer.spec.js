jest.mock("../../src/syncers/watcher", () => {
  function Watcher(k8sClient, gitLabClient) {
    this.k8sClient = k8sClient;
    this.gitLabClient = gitLabClient;
  }

  Watcher.prototype.start = jest.fn();
  Watcher.prototype.stop = jest.fn();
  return Watcher;
});

const KeySyncer = require("../../src/syncers/keySyncer");

const PUBLIC_KEY = "an-invalid-public-ssh-key";
let keySyncer, k8sClient, gitLabClient, repoEvent;

beforeEach(() => {
  k8sClient = {
    getSecretData: jest.fn(),
    addAnnotationsToCustomObject: jest.fn(),
  };

  k8sClient.getSecretData.mockResolvedValue({
    "identity.pub": Buffer.from(PUBLIC_KEY, "utf8").toString("base64"),
  });

  gitLabClient = {
    addKey: jest.fn(),
    removeKey: jest.fn(),
  };

  keySyncer = new KeySyncer(k8sClient, gitLabClient);

  repoEvent = JSON.parse(JSON.stringify(require("./events/gitrepository.json")));
});

it("constructs correctly", () => {
  expect(keySyncer.k8sClient).toBe(k8sClient);
  expect(keySyncer.gitLabClient).toBe(gitLabClient);
  expect(keySyncer.isListening).toBe(false);
  expect(keySyncer.name).toBe("KeySyncer");
});

it("starts and stops correctly", () => {
  keySyncer.start();
  expect(keySyncer.isListening).toBe(true);
  keySyncer.stop();
  expect(keySyncer.isListening).toBe(false);

  expect(keySyncer.watcher.start).toHaveBeenCalled();
  expect(keySyncer.watcher.stop).toHaveBeenCalled();
});

it("updates listening state during an onClose event", () => {
  keySyncer.start();
  expect(keySyncer.isListening).toBe(true);

  keySyncer.onClose();

  expect(keySyncer.isListening).toBe(false);
});

describe("event handling", () => {
  it("ignores non-delete events for annotated repos", async () => {
    repoEvent.metadata.annotations["platform.it.vt.edu/key-synced"] = "completed";

    await keySyncer.onEvent("POLL", repoEvent);
  });

  it("skips events for repos that don't have a secret", async () => {
    delete repoEvent.spec.secretRef;

    await keySyncer.onEvent("POLL", repoEvent);
  });

  it("adds the key as expected", async () => {
    await keySyncer.onEvent("POLL", repoEvent);

    expect(k8sClient.getSecretData).toHaveBeenCalled();
    expect(gitLabClient.addKey).toHaveBeenCalledWith("test/flux-sync-test", "flux-gitlab-key-syncer", PUBLIC_KEY);
    expect(k8sClient.addAnnotationsToCustomObject).toHaveBeenCalledWith(
      "source.toolkit.fluxcd.io",
      "v1",
      "gitrepositories",
      "test",
      "325931",
      { "platform.it.vt.edu/key-synced": "completed" }
    );
  });

  it("deletes the key with a deletion event", async () => {
    await keySyncer.onEvent("DELETED", repoEvent);

    expect(gitLabClient.removeKey).toHaveBeenCalledWith("test/flux-sync-test", PUBLIC_KEY);
  });

  it("deletes the deploy key with a deletion event for an annotated object", async () => {
    repoEvent.metadata.annotations["platform.it.vt.edu/key-synced"] = "completed";

    await keySyncer.onEvent("DELETED", repoEvent);

    expect(gitLabClient.removeKey).toHaveBeenCalledWith("test/flux-sync-test", PUBLIC_KEY);
  });
});
