const Util = require("../src/util");

describe("extractGitRepoFromUrl", () => {
  it("works with ssh clone URLs", () => {
    const repoUrl = "ssh://git@code.vt.edu/test/flux-sync-test";
    expect(Util.extractGitRepoFromUrl(repoUrl)).toBe("test/flux-sync-test");
  });

  it("works with ssh clone URLs with git extension at the end", () => {
    const repoUrl = "ssh://git@code.vt.edu/test/flux-sync-test.git";
    expect(Util.extractGitRepoFromUrl(repoUrl)).toBe("test/flux-sync-test");
  });
});
