const fs = require("fs");
const path = require("path");
const { Gitlab } = require("@gitbeaker/node");
const GitLabClient = require("../../src/clients/gitLab");
const { fail } = require("assert");

jest.mock("@gitbeaker/node", () => ({
  Gitlab: jest.fn(),
}));

const PROJECT_NAME = "group/sub-group/project-name";
const KEY_NAME = "test-key";
const PUBLIC_KEY =
  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDl8gDDmEQHeqfV1zvMe48RUoedhDuGGiEN20k5vbzYpx0DCQnJpZUXXs5wWcnBWrS66Hm8QBry2pN8sPAVK1IQ+ysFF5HP1LTLaNLtVmt/r4waZOoW8KfMw/7ovDN53nS+G04yxQ2NB6c+JriR6gaV+0IMWZrWE0WQFiMCac8/YCZ8hydH6qC9C/ESi2ui02iA5rjIHpK/sksIh0DlKwxQIg20Hbw6pWxRvEhSRTaPXwhsI1H3s/UCgHtTtr/uXr0cI4PSv9EZ5tBz3UR+FV9SjuOlG/QBdnJyTnVmnU/8nsk0X1D5edtN3w9/ErdkUbIoQrn7vmMvIlJwuaHggKm+Qc7HjmQ6zCnOTBfUR2tEkKIXQc/FCHrQ/a10yoauLP5moIQ2kSezG+PB6G0ptpywWiZHtIwJA31/B0d9ZdoVbtc7+sYKoXvei6QGNuQHfx95+2WsJVD8O6aime9g1WsoAcjZrC190NIdaZvK0/v1TWzBtr8wyoGYYcwTK4IAB7M= root@flux-768dc595fc-nthx6";
const WEBHOOK_URL = "https://notification.example.com/path/to/hook";
const WEBHOOK_TOKEN = "a-token";

afterEach(() => {
  delete process.env.GITLAB_HOST;
  delete process.env.GITLAB_PRIVATE_KEY_FILE;
  if (fs.existsSync(path.resolve() + "/gitlab-test.key")) fs.unlinkSync(path.resolve() + "/gitlab-test.key");
});

describe("constructor", () => {
  it("uses the api host and token when both defined", () => {
    new GitLabClient("https://gitlab.example.com", "token");
    expect(Gitlab).toHaveBeenCalledWith({ token: "token", host: "https://gitlab.example.com" });
  });

  it("uses default api host when not defined", () => {
    new GitLabClient(undefined, "token");
    expect(Gitlab).toHaveBeenCalledWith({ token: "token", host: "https://code.vt.edu" });
  });

  it("uses the environment variable for api host if none provided", () => {
    process.env.GITLAB_HOST = "https://gitlab.example.com";
    new GitLabClient(undefined, "token");
    expect(Gitlab).toHaveBeenCalledWith({ token: "token", host: "https://gitlab.example.com" });
  });

  it("uses the token from a file if one found", () => {
    const filePath = path.resolve() + "/gitlab-test.key";
    process.env.GITLAB_PRIVATE_KEY_FILE = filePath;
    fs.writeFileSync(filePath, "sample-token");

    new GitLabClient();
    expect(Gitlab).toHaveBeenCalledWith({ token: "sample-token", host: "https://code.vt.edu" });
  });

  it("trims the whitespace surrounding the token", () => {
    const filePath = path.resolve() + "/gitlab-test.key";
    process.env.GITLAB_PRIVATE_KEY_FILE = filePath;
    fs.writeFileSync(filePath, "sample-token\n");

    new GitLabClient();
    expect(Gitlab).toHaveBeenCalledWith({ token: "sample-token", host: "https://code.vt.edu" });
  });

  it("throws when no token provided and file not found", () => {
    process.env.GITLAB_PRIVATE_KEY_FILE = "test.key";
    try {
      new GitLabClient();
      fail("Should have thrown");
    } catch (err) {
      expect(err.message).toContain("Unable to find test.key");
    }
  });

  it("throws when no token provided and no env var set", () => {
    try {
      new GitLabClient();
      fail("Should have thrown");
    } catch (err) {
      expect(err.message).toContain("No GITLAB_PRIVATE_KEY_FILE defined");
    }
  });
});

describe("methods", () => {
  let client;

  beforeEach(() => {
    client = new GitLabClient("host", "token");

    client.client.DeployKeys = {
      all: jest.fn(),
      add: jest.fn(),
      remove: jest.fn(),
    };

    client.client.ProjectHooks = {
      edit: jest.fn(),
      add: jest.fn(),
      all: jest.fn(),
      remove: jest.fn(),
    };

    client.client.Projects = {
      search: jest.fn(),
    };
  });

  describe("addKey", () => {
    it("adds the key correctly when everything is good", async () => {
      client.client.DeployKeys.all.mockResolvedValue([]);

      await client.addKey(PROJECT_NAME, KEY_NAME, PUBLIC_KEY);

      expect(client.client.DeployKeys.all).toHaveBeenCalledWith({ projectId: PROJECT_NAME });
      expect(client.client.DeployKeys.add).toHaveBeenCalledWith(PROJECT_NAME, {
        title: KEY_NAME,
        key: PUBLIC_KEY,
        canPush: true,
      });
    });

    it("reverts to looking up the project id if unable to add the key directly", async () => {
      client.client.DeployKeys.all.mockResolvedValue([]);
      client.client.DeployKeys.add
        .mockImplementationOnce(() => Promise.reject(new Error("404 not found")))
        .mockImplementationOnce(() => ({}));
      client.client.Projects.search.mockResolvedValue([{ path_with_namespace: PROJECT_NAME, id: 123 }]);

      await client.addKey(PROJECT_NAME, KEY_NAME, PUBLIC_KEY);

      expect(client.client.DeployKeys.all).toHaveBeenCalledWith({ projectId: PROJECT_NAME });
      expect(client.client.DeployKeys.add).toHaveBeenCalledWith(123, {
        title: KEY_NAME,
        key: PUBLIC_KEY,
        canPush: true,
      });
    });

    it("throws if it's unable to find the project id", async () => {
      client.client.DeployKeys.all.mockResolvedValue([]);
      client.client.DeployKeys.add.mockImplementationOnce(() => Promise.reject(new Error("404 not found")));
      client.client.Projects.search.mockResolvedValue([]);

      try {
        await client.addKey(PROJECT_NAME, KEY_NAME, PUBLIC_KEY);
        fail("should have thrown");
      } catch (err) {
        expect(err.message).toContain("Unable to find GitLab project ID");
      }

      expect(client.client.DeployKeys.all).toHaveBeenCalledWith({ projectId: PROJECT_NAME });
      expect(client.client.DeployKeys.add).toHaveBeenCalledTimes(1);
    });

    it("throws if an unknown API error occurs", async () => {
      client.client.DeployKeys.all.mockResolvedValue([]);
      client.client.DeployKeys.add.mockImplementationOnce(() => Promise.reject(new Error("Unknown error")));

      try {
        await client.addKey(PROJECT_NAME, KEY_NAME, PUBLIC_KEY);
        fail("should have thrown");
      } catch (err) {
        expect(err.message).toContain("Unknown error");
      }
    });
  });

  describe("removeKey", () => {
    it("removes keys when key is found", async () => {
      client.client.DeployKeys.all.mockResolvedValue([{ title: KEY_NAME, id: 123, key: PUBLIC_KEY }]);

      await client.removeKey(PROJECT_NAME, PUBLIC_KEY);

      expect(client.client.DeployKeys.all).toHaveBeenCalledWith({ projectId: PROJECT_NAME });
      expect(client.client.DeployKeys.remove).toHaveBeenCalledWith(PROJECT_NAME, 123);
    });

    it("fails quietly when no keys found", async () => {
      client.client.DeployKeys.all.mockResolvedValue([]);

      await client.removeKey(PROJECT_NAME, KEY_NAME);

      expect(client.client.DeployKeys.all).toHaveBeenCalledWith({ projectId: PROJECT_NAME });
      expect(client.client.DeployKeys.remove).not.toHaveBeenCalled();
    });

    it("fails quietly no matching key is found", async () => {
      client.client.DeployKeys.all.mockResolvedValue([{ title : KEY_NAME, id : 123, key: "another-key" }]);

      await client.removeKey(PROJECT_NAME, KEY_NAME);

      expect(client.client.DeployKeys.all).toHaveBeenCalledWith({ projectId: PROJECT_NAME });
      expect(client.client.DeployKeys.remove).not.toHaveBeenCalled();
    });
  });

  describe("addWebhook", () => {
    it("adds webhook with token when no existing webhook is found", async () => {
      client.client.ProjectHooks.all.mockResolvedValue([]);
      client.client.ProjectHooks.add.mockResolvedValue({});

      await client.addWebhook(PROJECT_NAME, WEBHOOK_URL, WEBHOOK_TOKEN);

      expect(client.client.ProjectHooks.add).toHaveBeenCalledWith(PROJECT_NAME, WEBHOOK_URL, {
        push_events: true,
        token: WEBHOOK_TOKEN,
      });
    });

    it("updates webhook with token when existing webhook is found", async () => {
      client.client.ProjectHooks.all.mockResolvedValue([
        {
          id: 123,
          url: WEBHOOK_URL,
        },
      ]);
      client.client.ProjectHooks.edit.mockResolvedValue({});

      await client.addWebhook(PROJECT_NAME, WEBHOOK_URL, WEBHOOK_TOKEN);

      expect(client.client.ProjectHooks.add).not.toHaveBeenCalled();
      expect(client.client.ProjectHooks.edit).toHaveBeenCalledWith(PROJECT_NAME, 123, WEBHOOK_URL, {
        push_events: true,
        token: WEBHOOK_TOKEN,
      });
    });
  });

  describe("removeWebhook", () => {
    it("removes the webhook when existing webhook is found", async () => {
      client.client.ProjectHooks.all.mockResolvedValue([
        {
          id: 123,
          url: WEBHOOK_URL,
        },
      ]);
      client.client.ProjectHooks.remove.mockResolvedValue({});

      await client.removeWebhook(PROJECT_NAME, WEBHOOK_URL);

      expect(client.client.ProjectHooks.add).not.toHaveBeenCalled();
      expect(client.client.ProjectHooks.edit).not.toHaveBeenCalled();
      expect(client.client.ProjectHooks.remove).toHaveBeenCalledWith(PROJECT_NAME, 123);
    });

    it("works when no matching webhook is found", async () => {
      client.client.ProjectHooks.all.mockResolvedValue([]);

      await client.removeWebhook(PROJECT_NAME, WEBHOOK_URL);

      expect(client.client.ProjectHooks.remove).not.toHaveBeenCalled();
    });
  });
});
