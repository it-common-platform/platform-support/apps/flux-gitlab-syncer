FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/node:lts-alpine AS base
WORKDIR /app
COPY package.json yarn.lock ./

FROM base AS test
RUN yarn install 
COPY test ./test
COPY src ./src
RUN yarn test-coverage

FROM base AS prod
ENV NODE_ENV production
RUN yarn install --production \
    && yarn cache clean
COPY --from=test /app/src ./src
EXPOSE 8080
CMD ["node", "/app/src/index.js"]
