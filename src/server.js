const express = require("express");
const prometheusClient = require("./clients/prometheus");
const logger = require("./logger");

function Server(syncers, port = 8080) {
  this.syncers = syncers;
  this.port = port;
  this.server = express();
}

Server.prototype.start = function () {
  this.server.get("/healthz", (req, res) => {
    const status = (this.syncers.find(s => !s.isListening)) ? 500 : 200;
    const syncerReport = this.syncers.map(s => ({ name : s.name, listening : s.isListening }));

    res.status(status).send({
      syncers : syncerReport,
    });
  });

  this.server.get("/metrics", async (req, res) => {
    res.set("Content-Type", prometheusClient.register.contentType);
    res.end(prometheusClient.register.metrics());
  });

  return new Promise((acc) => {
    this.httpServer = this.server.listen(this.port, () => {
      logger.info(`Web server listening on port ${this.port}`);
      acc();
    });
  });
};

Server.prototype.stop = function () {
  return new Promise((acc) => {
    if (this.httpServer) this.httpServer.close(acc);
    else acc();
  });
};

module.exports = {
  Server,
};
