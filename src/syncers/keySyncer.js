const Watcher = require("./watcher");
const logger = require("../logger");
const Util = require("../util");

const KEY_SYNCED_ANNOTATION = "platform.it.vt.edu/key-synced";
const API_GROUP = "source.toolkit.fluxcd.io";
const API_VERSION = "v1beta1";
const KIND_PLURAL = "gitrepositories";

function KeySyncer(k8sClient, gitLabClient, keyIdentifier = "flux-gitlab-key-syncer") {
  this.watcher = new Watcher(k8sClient, API_GROUP, API_VERSION, KIND_PLURAL);

  this.gitLabClient = gitLabClient;
  this.k8sClient = k8sClient;
  this.isListening = false;
  this.keyIdentifier = keyIdentifier;

  this.name = "KeySyncer";
}

KeySyncer.prototype.start = function () {
  this.watcher.start(this.onEvent.bind(this), this.onClose.bind(this));
  this.isListening = true;
};

KeySyncer.prototype.stop = function () {
  this.watcher.stop();
  this.isListening = false;
};

KeySyncer.prototype.onClose = function() {
  this.isListening = false;
  // Could eventually try to retry to get the watcher going again
}

KeySyncer.prototype.onEvent = async function (eventType, gitRepository) {
  if (eventType !== "DELETED" && gitRepository.metadata.annotations && gitRepository.metadata.annotations[KEY_SYNCED_ANNOTATION]) return;

  if (!gitRepository.spec.secretRef) {
    return logger.info(`GitRepository ${gitRepository.metadata.name} has no secretRef. Skipping sync`);
  }

  const sshKeyInfo = await this.k8sClient.getSecretData(gitRepository.spec.secretRef.name);
  const publicKey = Buffer.from(sshKeyInfo["identity.pub"], "base64").toString();

  const projectPath = Util.extractGitRepoFromUrl(gitRepository.spec.url);

  if (eventType === "DELETED") {
    await this.gitLabClient.removeKey(projectPath, publicKey);
  } else {
    await this.gitLabClient.addKey(projectPath, this.keyIdentifier, publicKey);

    await this.k8sClient.addAnnotationsToCustomObject(
      gitRepository.apiVersion.split("/")[0],
      gitRepository.apiVersion.split("/")[1],
      KIND_PLURAL, // hard-coded because we need the plural version
      gitRepository.metadata.name,
      gitRepository.metadata.resourceVersion,
      {
        [KEY_SYNCED_ANNOTATION]: "completed",
      }
    );
  }
};

module.exports = KeySyncer;
