/**
 * A Watcher encompasses the ability to both watch for events about specific objects and occasional polling. Anytime an event
 * occurs or the polling triggers, the callback is notified with the updated object.
 *
 * @param k8sClient The client to use to communicate with the Kubernetes API
 * @param pollPeriod The polling period in millis, defaulting to 5 minutes
 * @constructor
 */
function Watcher(k8sClient, group, version, plural, pollPeriod = 300000) {
  this.k8sClient = k8sClient;
  this.group = group;
  this.version = version;
  this.plural = plural;
  this.pollPeriod = pollPeriod;
}

Watcher.prototype.start = function (callback, closeCallback) {
  this.callback = callback;
  this.pollTimer = setTimeout(() => this.poll(), this.pollPeriod);

  return this.k8sClient
    .watchCustomResource(this.group, this.version, this.plural, callback, closeCallback)
    .then((req) => (this.watchRequest = req));
};

Watcher.prototype.stop = function () {
  if (this.watchRequest) {
    this.watchRequest.abort();
    this.watchRequest = undefined;
  }

  if (this.pollTimer) {
    clearTimeout(this.pollTimer);
    this.pollTimer = undefined;
  }
};

Watcher.prototype.poll = function () {
  this.k8sClient
    .listCustomResources(this.group, this.version, this.plural)
    .then((resources) => {
      resources.forEach((r) => this.callback("POLL", r));
    })
    .finally(() => {
      this.pollTimer = setTimeout(() => this.poll(), this.pollPeriod);
    });
};

module.exports = Watcher;
