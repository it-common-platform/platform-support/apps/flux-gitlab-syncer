const Watcher = require("./watcher");
const logger = require("../logger");
const Util = require("../util");

const WEBHOOK_SYNCED_ANNOTATION = "platform.it.vt.edu/webhook-synced";
const PROJECT_PATH_ANNOTATION = "platform.it.vt.edu/webhook-sync-project";
const API_GROUP = "notification.toolkit.fluxcd.io";
const API_VERSION = "v1";
const KIND_PLURAL = "receivers";

function WebhookSyncer(k8sClient, webhookBaseUrl, gitLabClient) {
  this.k8sClient = k8sClient;

  this.watcher = new Watcher(k8sClient, API_GROUP, API_VERSION, KIND_PLURAL);

  this.webhookBaseUrl = webhookBaseUrl;
  this.gitLabClient = gitLabClient;
  this.k8sClient = k8sClient;

  this.name = "WebhookSyncer";
  this.isListening = false;

  logger.info(`Configuring WebhookSyncer to use base URL of ${this.webhookBaseUrl}`);
}

WebhookSyncer.prototype.start = function () {
  this.watcher.start(this.onEvent.bind(this), this.onClose.bind(this));
  this.isListening = true;
};

WebhookSyncer.prototype.stop = function () {
  this.watcher.stop();
  this.isListening = false;
};

WebhookSyncer.prototype.onClose = function() {
  this.isListening = false;
  // Can eventually try to retry the connection
}

WebhookSyncer.prototype.onEvent = async function (eventType, receiver) {
  if (eventType !== "DELETED" && receiver.metadata.annotations && receiver.metadata.annotations[WEBHOOK_SYNCED_ANNOTATION]) return;

  if (!receiver.status || !receiver.status.webhookPath) {
    return logger.warn(`Receiver ${receiver.metadata.name} doesn't have status or is missing webhookPath. status: ${receiver.status}`);
  }

  if (receiver.spec.resources[0].kind !== "GitRepository") {
    return logger.warn(`Non-GitRepository resource found on receiver ${receiver.metadata.name}`);
  }

  if (eventType === "DELETED") {
    return this._removeWebhook(receiver);
  } else {
    return this._addWebhook(receiver);
  }
};

WebhookSyncer.prototype._addWebhook = async function (receiver) {
  let webhookToken;
  if (receiver.spec.secretRef) {
    const webhookSecret = await this.k8sClient.getSecretData(receiver.spec.secretRef.name);
    webhookToken = Buffer.from(webhookSecret.token, "base64").toString();
  }

  const webhookUrl = `${this.webhookBaseUrl}${receiver.status.webhookPath}`;

  // Hard-coding the "kind" because only the singular form of the resource comes in the event and
  // the API endpoints need the plural form (gitrepository vs gitrepositories)
  const { apiVersion, name } = receiver.spec.resources[0];
  const gitRepoDetails = await this.k8sClient.getCustomResource(
    apiVersion.split("/")[0],
    apiVersion.split("/")[1],
    "gitrepositories",
    name
  );
  const projectPath = Util.extractGitRepoFromUrl(gitRepoDetails.spec.url);

  await this.gitLabClient.addWebhook(projectPath, webhookUrl, webhookToken);

  // We add the project path as an annotation to help with removal, as the
  // GitRepository may be removed and inaccessible to lookup the path.
  const annotations = {
    [PROJECT_PATH_ANNOTATION]: projectPath,
    [WEBHOOK_SYNCED_ANNOTATION]: "completed",
  };

  await this.k8sClient.addAnnotationsToCustomObject(
    API_GROUP,
    API_VERSION,
    KIND_PLURAL,
    receiver.metadata.name,
    receiver.metadata.resourceVersion,
    annotations
  );
};

WebhookSyncer.prototype._removeWebhook = async function (receiver) {
  const webhookUrl = `${this.webhookBaseUrl}${receiver.status.webhookPath}`;
  const projectPath = receiver.metadata.annotations[PROJECT_PATH_ANNOTATION];
  await this.gitLabClient.removeWebhook(projectPath, webhookUrl);
};

module.exports = WebhookSyncer;
