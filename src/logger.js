function debug() {
  console.debug("[DEBUG] ", new Date(), ...arguments);
}

function info() {
  console.log("[INFO] ", new Date(), ...arguments);
}

function warn() {
  console.warn("[WARN] ", new Date(), ...arguments);
}

function error() {
  console.error("[ERROR] ", new Date(), ...arguments);
}

module.exports = {
  debug,
  info,
  warn,
  error,
};
