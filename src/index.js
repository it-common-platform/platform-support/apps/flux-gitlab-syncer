const K8sClient = require("./clients/k8s");
const GitLabClient = require("./clients/gitLab");
const KeySyncer = require("./syncers/keySyncer");
const WebhookSyncer = require("./syncers/webhookSyncer");
const { Server } = require("./server");
const logger = require("./logger");

let keySyncer, webhookSyncer, server;

async function run() {
  if (!process.env.WEBHOOK_BASE_URL) {
    throw new Error("WEBHOOK_BASE_URL must be defined");
  }

  if (!process.env.NAMESPACE) {
    throw new Error("NAMESPACE must be defined");
  }

  const k8sClient = new K8sClient(process.env.NAMESPACE);
  const gitLabClient = new GitLabClient();
  await gitLabClient.validate();

  keySyncer = new KeySyncer(k8sClient, gitLabClient, process.env.DEPLOY_KEY_IDENTIFIER);
  await keySyncer.start();

  webhookSyncer = new WebhookSyncer(k8sClient, process.env.WEBHOOK_BASE_URL, gitLabClient);
  await webhookSyncer.start();

  server = new Server(
    [ keySyncer, webhookSyncer ],
    process.env.PORT || 8080
  );

  server.start();
}

async function shutdown() {
  try {
    if (keySyncer) {
      await keySyncer.stop();
    }

    if (server) {
      await server.stop();
    }

    process.exit(0);
  } catch (err) {
    process.exit(1);
  }
}

run().catch((err) => {
  logger.error(err.message);
  throw err;
});

process.on("SIGINT", shutdown);
process.on("SIGTERM", shutdown);
