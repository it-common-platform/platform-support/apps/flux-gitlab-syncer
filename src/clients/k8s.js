const k8s = require("@kubernetes/client-node");
const logger = require("../logger");

function K8sClient(namespace, kubeConfig) {
  if (!kubeConfig) {
    kubeConfig = new k8s.KubeConfig();
    kubeConfig.loadFromDefault();
  }

  this.watcher = new k8s.Watch(kubeConfig);
  this.k8sCoreClient = kubeConfig.makeApiClient(k8s.CoreV1Api);
  this.customObjectClient = kubeConfig.makeApiClient(k8s.CustomObjectsApi);

  this.namespace = namespace;
}

K8sClient.prototype.listCustomResources = function (group, version, plural) {
  return this.customObjectClient
    .listNamespacedCustomObject(group, version, this.namespace, plural)
    .then((r) => r.body.items);
};

K8sClient.prototype.getCustomResource = function (group, version, plural, name) {
  return this.customObjectClient
    .getNamespacedCustomObject(group, version, this.namespace, plural, name)
    .then((r) => r.body)
    .catch((err) => {
      if (err.body.code === 404) return null;
      throw err;
    });
};

K8sClient.prototype.watchCustomResource = function (group, version, plural, eventCallback, onCloseCallback) {
  const url = this._createApiEndpointUrl(group, version, this.namespace, plural);
  return this.watcher.watch(
    url,
    {},
    (event, apiObj) => {
      if (!event || !apiObj)
        return logger.warn("Empty event data from the watch. May want to validate Kube API permissions");

      eventCallback(event, apiObj).catch((err) =>
        logger.error(`Got an unhandled error while processing event for ${event} : ${group}/${version}:${plural}`, err)
      );
    },
    onCloseCallback
  );
};

K8sClient.prototype.getSecretData = function (name) {
  return this.k8sCoreClient
    .readNamespacedSecret(name, this.namespace)
    .then((r) => r.body.data)
    .catch((err) => {
      console.log(`Error getting secret ${name}`);
      throw err;
    });
};

K8sClient.prototype._createApiEndpointUrl = function (group, version, namespace, plural) {
  return "/apis/{group}/{version}/namespaces/{namespace}/{plural}"
    .replace("{group}", encodeURIComponent(String(group)))
    .replace("{version}", encodeURIComponent(String(version)))
    .replace("{namespace}", encodeURIComponent(String(namespace)))
    .replace("{plural}", encodeURIComponent(String(plural)));
};

/**
 * Add an annotation to a pod
 * @param {string} name The name of the object to update
 * @param {string} resourceVersion The resource version of the object (comes from metadata)
 * @param {string} label The label of the annotation
 * @param {string} value The value of the annotation
 * @returns {Promise} Resolved when complete. Rejected on error.
 */
K8sClient.prototype.addAnnotationsToCustomObject = function (
  group,
  version,
  plural,
  name,
  resourceVersion,
  annotations
) {
  const patch = {
    metadata: {
      resourceVersion,
      annotations,
    },
  };

  return this.customObjectClient
    .patchNamespacedCustomObject(group, version, this.namespace, plural, name, patch, undefined, undefined, undefined, {
      headers: { "Content-Type": "application/merge-patch+json" },
    })
    .catch((err) => {
      if (err.body.code === 409) return;
      throw err;
    });
};

module.exports = K8sClient;
