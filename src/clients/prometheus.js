const prometheusClient = require("prom-client");

prometheusClient.collectDefaultMetrics();

module.exports = prometheusClient;
