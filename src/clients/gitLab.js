const fs = require("fs");
const { Gitlab } = require("@gitbeaker/node");
const logger = require("../logger");

function GitLabClient(apiHost, token) {
  if (!apiHost) {
    apiHost = process.env.GITLAB_HOST ? process.env.GITLAB_HOST : "https://code.vt.edu";
  }

  if (!token) {
    if (!process.env.GITLAB_PRIVATE_KEY_FILE) {
      throw new Error("No GITLAB_PRIVATE_KEY_FILE defined");
    }

    if (!fs.existsSync(process.env.GITLAB_PRIVATE_KEY_FILE)) {
      throw new Error(`Unable to find ${process.env.GITLAB_PRIVATE_KEY_FILE}`);
    }

    token = fs.readFileSync(process.env.GITLAB_PRIVATE_KEY_FILE).toString().trim();
  }

  logger.info(`Configured GitLab client to use ${apiHost} with token ${token.substr(0, 2)}****`);

  this.client = new Gitlab({
    token: token,
    host: apiHost,
  });
}

GitLabClient.prototype.validate = async function() {
  try {
    const currentUser = await this.client.Users.current();
    logger.info(`GitLab client configured and connected as '${currentUser.username}'`);
  } catch (err) {
    throw new Error("Unable to validate the GitLab client. Validate host and token are configured correctly")
  }
}

/**
 * Add a key to the specified project
 * @param {string} projectName The project name
 * @param {string} keyName The name of the key to create
 * @param {string} publicKey The public key itself
 */
GitLabClient.prototype.addKey = async function (projectName, keyName, publicKey) {
  logger.info(`Adding ${keyName} to ${projectName}: ${publicKey.substr(0, 20)}...`);

  if (await this.hasKey(projectName, publicKey))
    return;

  try {
    await this.client.DeployKeys.add(projectName, {
      title: keyName,
      key: publicKey,
      canPush: true,
    });
  } catch (err) {
    if (err.message.indexOf("404") > -1) {
      logger.info("Project not found using project name. Searching for project id");

      const projectSplit = projectName.split("/");

      const projects = await this.client.Projects.search(projectSplit[projectSplit.length - 1]);
      const matchingProject = projects.find((p) => p.path_with_namespace === projectName);
      if (!matchingProject) throw new Error(`Unable to find GitLab project ID with name ${projectName}`);

      return this.addKey(matchingProject.id, keyName, publicKey).catch((err) => {
        logger.error(`Error while trying to add deploy key to repo ${projectName}: ${err}`);
        throw err;
      });
    } else {
      logger.error(`Error while trying to add deploy key to ${projectName}`, err);
      throw err;
    }
  }

  logger.info(`Added key ${keyName} to ${projectName}`);
};

GitLabClient.prototype.hasKey = async function (projectName, publicKey) {
  try {
    const keys = await this.client.DeployKeys.all({ projectId: projectName });
    return keys.find(k => k.key === publicKey) !== undefined;
  } catch (err) {
    logger.error(`Error while trying to get keys for ${projectName}`);
    throw err;
  }
};

/**
 * Remove a key from the specified project
 * @param {string} projectName The project name
 * @param {string} publicKey The public key
 */
GitLabClient.prototype.removeKey = async function (projectName, publicKey) {
  const keys = await this.client.DeployKeys.all({ projectId: projectName });
  const matchingKey = keys.find((k) => k.key === publicKey);

  if (!matchingKey) {
    return logger.info(`Tried to remove key from ${projectName}, but not found`);
  }

  try {
    await this.client.DeployKeys.remove(projectName, matchingKey.id);
    logger.info(`Removed key ${keyName} from ${projectName}`);
  } catch (err) {
    logger.error(`Error while trying to remove key from ${projectName}`);
  }
};

GitLabClient.prototype.addWebhook = async function (projectName, url, token) {
  const existingWebhook = await this._findExistingWebhook(projectName, url);

  const options = {
    push_events: true,
  };

  if (token) {
    options.token = token;
  }

  if (existingWebhook) {
    logger.info(`Updating webhook for repo ${projectName}`);
    return this.client.ProjectHooks.edit(projectName, existingWebhook.id, url, options).catch((err) => {
      logger.error(`Error while trying to update webhook to repo ${projectName}: ${err}`);
      throw err;
    });
  }

  logger.info(`Adding webhook for repo ${projectName}`);
  return this.client.ProjectHooks.add(projectName, url, options).catch((err) => {
    logger.error(`Error while trying to add webhook to repo ${projectName}: ${err}`);
    throw err;
  });
};

GitLabClient.prototype.removeWebhook = async function (projectName, url) {
  const existingWebhook = await this._findExistingWebhook(projectName, url);
  if (!existingWebhook) return logger.info(`Unable to remove webhook for ${projectName}, as it was not found`);

  logger.info(`Deleting the webhook for ${projectName}`);
  return this.client.ProjectHooks.remove(projectName, existingWebhook.id).catch((err) => {
    logger.error(`Error while trying to remove webhook from repo ${projectName}: ${err}`);
    throw err;
  });
};

GitLabClient.prototype._findExistingWebhook = async function (projectName, url) {
  const webhooks = await this.client.ProjectHooks.all(projectName);
  return webhooks.find((hook) => hook.url === url);
};

module.exports = GitLabClient;
