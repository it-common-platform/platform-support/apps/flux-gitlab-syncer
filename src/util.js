function extractGitRepoFromUrl(url) {
  const parsedUrl = new URL(url);
  const pathname = parsedUrl.pathname.substr(1);
  return pathname.endsWith(".git") ? pathname.substr(0, pathname.length - 4) : pathname;
}

module.exports = {
  extractGitRepoFromUrl,
};
