const fs = require("fs");
const yamljs = require('js-yaml');
const k8s = require("@kubernetes/client-node");

function DeployUtil() {
  const kc = new k8s.KubeConfig();
  kc.loadFromDefault();

  this.k8sCoreApi = kc.makeApiClient(k8s.CoreV1Api);
  this.k8sAppsApi = kc.makeApiClient(k8s.AppsV1Api);
  this.k8sAuthApi = kc.makeApiClient(k8s.RbacAuthorizationV1Api);
  this.k8sCustomApi = kc.makeApiClient(k8s.CustomObjectsApi);
}

/**
 * Apply the manifest file. Currently only does create. Patch not supported.
 * @param {string} manifestFile The manifest file to apply
 */
DeployUtil.prototype.applyManifestFile = async function(manifestFile) {
  return this._operateOnFile('create', manifestFile);
}

/**
 * Delete the resources specified in the supplied manifest file.
 * @param {string} manifestFile The manifest file to delete
 */
DeployUtil.prototype.deleteManifestFile = async function(manifestFile) {
  return this._operateOnFile('delete', manifestFile);
};

/**
 * Wait until the provided service has active endpoints.
 * @param {string} namespace The namespace the service belongs to
 * @param {string} serviceName The name of the service
 * @param {number} timeout A timeout, in millis, to wait until giving up
 */
DeployUtil.prototype.waitUntilServiceHasEndpoints = function(namespace, serviceName, timeout=15000) {
  const checkForEndpoint = async () => {
    try {
      const response = await this.k8sCoreApi.readNamespacedEndpoints(serviceName, namespace);
      return response.body.subsets !== undefined && response.body.subsets[0].addresses !== undefined && response.body.subsets[0].addresses.length > 0;
    }
    catch (err) {
      return false;
    }
  };

  return this._runWithTimer(timeout, `Timed out waiting for endpoints on ${namespace}/${serviceName}`, checkForEndpoint);
};

/**
 * Wait until no pods exist in the specified namespace given the provided labels
 * @param {string} namespace The namespace to ensure no pods exist in
 * @param {object} labelSelector The labels to filter on
 * @param {number} timeout A timeout, in millis, to wait until giving up
 */
DeployUtil.prototype.waitForNoPods = function(namespace, labelSelector, timeout=15000) {
  const selector = Object
    .keys(labelSelector)
    .map((key) => `${key}=${labelSelector[key]}`)
    .join(",");

  const checkForPods = async () => {
    const pods = await this.k8sCoreApi.listNamespacedPod(namespace, undefined, undefined, undefined, undefined, selector);
    return pods.body.items.length === 0;
  };

  return this._runWithTimer(timeout, `Timed out waiting for no pods in ${namespace}`, checkForPods);
}

DeployUtil.prototype._runWithTimer = function(timeout, timeoutErrorMessage, callback) {
  let timeoutTimer;
  return new Promise((acc, rej) => {
    timeoutTimer = setTimeout(() => rej(new Error(timeoutErrorMessage)), timeout);

    const runCheck = async() => {
      if (!timeoutTimer) return;

      try {
        const done = await callback();
        if (done) return acc();
      } catch (err) {
        console.log(err);
        // Don't worry for now...
      }
      setTimeout(runCheck, 1000);
    };

    runCheck();
  }).finally(() => {
    if (timeoutTimer) {
      clearTimeout(timeoutTimer)
      timeoutTimer = null;
    }
  });
}


/**
 * Read the manifest file and perform the specified operation (create or delete)
 * @param {string} operation The operation to perform (create or delete)
 * @param {string} file The manifest file
 * @private
 */
DeployUtil.prototype._operateOnFile = async function(operation, file) {
  const data = fs.readFileSync(file).toString();
  const objects = yamljs.loadAll(data);

  for (let i = 0; i < objects.length; i++) {
    try {
      await this._doOperation(operation, objects[i]);
    } catch (err) {
      if (operation === "delete" && err.response.body.code === 404)
        continue;

      if (err.response && err.response.body && err.response.body.code) {
        const errorMessage = `Error during ${operation}: ${err.response.body.code} - ${err.response.body.message}`;
        throw new Error(errorMessage);
      }
      throw err;
    }
  }
};

/**
 * Actually do the operation. This figures out and uses the correct api client.
 * @param {string} operation The operation to perform (create or delete)
 * @param {object} object The k8s object (from the manifest) to operate on
 * @private
 */
DeployUtil.prototype._doOperation = function(operation, object) {
  let operator, funcName = `${operation}Namespaced${object.kind}`;
  const namespace = (object.metadata && object.metadata.namespace) ? object.metadata.namespace : "default";
  let funcArgs = (operation === "create") ? [namespace, object] : [object.metadata.name, namespace];

  if (object.apiVersion.indexOf("flux") > -1) {
    const [ group, apiVersion ] = object.apiVersion.split("/");
    const plural = (object.kind.endsWith("y")) ?
      (object.kind.toLowerCase().substring(0, object.kind.length - 1) + "ies") :
      `${object.kind.toLowerCase()}s`;

    operator = this.k8sCustomApi;
    funcName = `${operation}NamespacedCustomObject`;
    funcArgs = (operation === "create") ?
      [ group, apiVersion, "default", plural, object ] :
      [ group, apiVersion, "default", plural, object.metadata.name ];
  }
  else {
    switch (object.apiVersion) {
      case "v1":
        operator = this.k8sCoreApi;
        break;
      case "apps/v1":
        operator = this.k8sAppsApi;
        break;
      case "rbac.authorization.k8s.io/v1":
        operator = this.k8sAuthApi;
        if (object.kind.startsWith("Cluster")) {
          funcName = `${operation}${object.kind}`;
          funcArgs = (operation === "create") ? [object] : [object.metadata.name];
        }
        break;
      default:
        throw new Error(`Don't know how to work with ${object.apiVersion}`);
    }
  }

  return operator[funcName].apply(operator, funcArgs);
}



module.exports = DeployUtil;
