const mockServer = require("mockserver-client");
const DeployUtil = require("./deployUtil");
const fetch = require("node-fetch");
const AbortController = require("abort-controller");

const MOCKSERVER_HOST = "mockserver";

const PUBLIC_KEY = "AAAAB3NzaC1yc2EAAAABJQAAAIEAiPWx6WM4lhHNedGfBpPJNPpZ7yKu+dnn1SJejgt4596k6YjzGGphH2TUxwKzxcKDKKezwkpfnxPkSMkuEspGRt/aZZ9wa++Oi7Qkr8prgHc4soW6NUlfDzpvZK2H5E7eQaSeP3SAwGmQKUFHCddNaP0L+hM7zhFNzjFvpaMgJw0=";

const MOCK_REQUESTS = {
  validateGitLabToken: {
    httpRequest: {
      method: "GET",
      path: "/api/v4/user"
    },
    httpResponse: {
      statusCode: 200,
        body: JSON.stringify({
        username: "flux-e2e-tests"
      })
    }
  },
  getDeployKeys: {
    httpRequest: {
      method: "GET",
      path: "/api/v4/projects/flux-key-syncer%2Ftests%2Fend-to-end/deploy_keys",
    },
    httpResponse: {
      statusCode: 200,
      body: JSON.stringify([]),
    }
  },
  addDeployKeyToProject: {
    httpRequest: {
      method: "POST",
      path: "/api/v4/projects/flux-key-syncer%2Ftests%2Fend-to-end/deploy_keys",
    },
    httpResponse: {
      statusCode: 200,
      body: JSON.stringify({
        "key" : PUBLIC_KEY,
        "id" : 123,
        "title" : "My deploy key",
        "can_push": true,
        "created_at" : "2015-08-29T12:44:31.550Z"
      })
    }
  },
  removeDeployKeyFromProject: {
    httpRequest: {
      method: "DELETE",
      path: "/api/v4/projects/flux-key-syncer%2Ftests%2Fend-to-end/deploy_keys/123",
    },
    httpResponse: {
      statusCode: 200,
    },
  },
  getWebhooksForProjectWhenEmpty: {
    httpRequest: {
      method: "GET",
      path: "/api/v4/projects/flux-key-syncer%2Ftests%2Fend-to-end/hooks",
    },
    httpResponse: {
      statusCode: 200,
      body: JSON.stringify([]),
    }
  },
  addWebhookToProject: {
    httpRequest: {
      method: "POST",
      path: "/api/v4/projects/flux-key-syncer%2Ftests%2Fend-to-end/hooks",
    },
    httpResponse: {
      statusCode: 200,
      body: JSON.stringify({
        id: 321,
        url: "http://mockserver:1080/something"
      })
    }
  },
  removeWebhookFromProject: {
    httpRequest: {
      method: "DELETE",
      path: "/api/v4/projects/flux-key-syncer%2Ftests%2Fend-to-end/hooks/234",
    },
    httpResponse: {
      statusCode: 200
    }
  }
};

let deployUtil;

// Start the mockserver and wait for it to start
beforeAll(async () => {
  deployUtil = new DeployUtil();

  console.log("Deploying mockserver");
  await deployUtil.applyManifestFile("./manifests/mockserver.yaml");

  console.log("Waiting for mockserver to start up...");
  await deployUtil.waitUntilServiceHasEndpoints("default", "mockserver", 15000);

  // The HTTP server responds, but isn't actually ready to work for a little bit
  await deployUtil._runWithTimer(30000, "Unable to get mockserver to start", async () => {
    const controller = new AbortController();
    const timeout = setTimeout(() => controller.abort(), 750);

    try {
      const response = await fetch("http://mockserver:1080/status", { signal: controller.signal });
      return response.ok;
    }
    catch (e) {}
    finally {
      clearTimeout(timeout);
    }
  });
}, 60000);

beforeEach(async () => {
  const mockServerClient = mockServer.mockServerClient(MOCKSERVER_HOST, 1080);
  await mockServerClient.mockAnyResponse(MOCK_REQUESTS.validateGitLabToken);
}, 10000);

// Deploy the syncer and wait for it to be up and started
beforeEach(async () => {
  console.log("Deploying the syncer and waiting to start...");

  await deployUtil.applyManifestFile("./manifests/secrets.yaml");
  await deployUtil.applyManifestFile("./manifests/syncer.yaml");

  await deployUtil.waitUntilServiceHasEndpoints("default", "flux-gitlab-syncer", 15000);
}, 30000);

// Cleanup by removing the secrets and syncer
afterEach(async () => {
  const mockServerClient = mockServer.mockServerClient(MOCKSERVER_HOST, 1080);
  await mockServerClient.reset();

  console.log("Cleaning up after the test...");
  await deployUtil.deleteManifestFile("./manifests/gitrepository.yaml");
  await deployUtil.deleteManifestFile("./manifests/receiver.yaml");
  await deployUtil.deleteManifestFile("./manifests/secrets.yaml");
  await deployUtil.deleteManifestFile("./manifests/syncer.yaml");

  await deployUtil.waitForNoPods("default", { app: "flux-gitlab-syncer" }, 30000);
}, 45000);


//////////////////////////////// ACTUAL TESTS ////////////////////////////////


it("adds deploy key when GitRepository is added", async () => {
  const mockServerClient = mockServer.mockServerClient(MOCKSERVER_HOST, 1080);
  await mockServerClient.mockAnyResponse(MOCK_REQUESTS.getDeployKeys);
  await mockServerClient.mockAnyResponse(MOCK_REQUESTS.addDeployKeyToProject);

  console.log("Adding GitRepository now");
  await deployUtil.applyManifestFile("./manifests/gitrepository.yaml");

  await waitForRequest(mockServerClient, MOCK_REQUESTS.addDeployKeyToProject.httpRequest, 5000);
}, 30000);

it("adds webhook when a Receiver is added", async () => {
  const mockServerClient = mockServer.mockServerClient(MOCKSERVER_HOST, 1080);
  await mockServerClient.mockAnyResponse(MOCK_REQUESTS.getDeployKeys);
  await mockServerClient.mockAnyResponse(MOCK_REQUESTS.addDeployKeyToProject);
  await mockServerClient.mockAnyResponse(MOCK_REQUESTS.getWebhooksForProjectWhenEmpty);
  await mockServerClient.mockAnyResponse(MOCK_REQUESTS.addWebhookToProject);

  console.log("Adding GitRepository and Receiver now");
  await deployUtil.applyManifestFile("./manifests/gitrepository.yaml");
  await deployUtil.applyManifestFile("./manifests/receiver.yaml");

  await waitForRequest(mockServerClient, MOCK_REQUESTS.addWebhookToProject.httpRequest, 20000);
}, 30000);

function waitForRequest(mockServerClient, requestDefinition, millis) {
  return new Promise((acc, rej) => {
    let timer = setTimeout(async () => {
      await mockServerClient.retrieveRecordedRequests();
      rej(new Error("Timed out waiting for expectations to be satisfied"));
      timer = null;
    }, millis);

    async function fetchRequests() {
      if (!timer) return;

      try {
        await mockServerClient.verify(requestDefinition);
        if (timer) {
          clearTimeout(timer);
          timer = null;
          acc();
        }
      } catch (err) {
        setTimeout(fetchRequests, 1000);
      }
    }

    fetchRequests();
  });
}
