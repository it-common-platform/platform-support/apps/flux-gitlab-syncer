#!/bin/sh

if [ "$SYNCER_IMAGE" == "" ]; then
  echo "No SYNCER_IMAGE defined"
  exit 1
fi

if [ "$SYNCER_TESTS_IMAGE" == "" ]; then
  echo "No SYNCER_TESTS_IMAGE defined"
  exit 1
fi

set -e

CLUSTER_NAME=flux-gitlab-syncer-tests
mockserverImageTag=${MOCKSERVER_IMAGE_TAG:-mockserver-5.11.2}
kubeApiServerHost=${KUBE_API_SERVER_HOST:-localhost}

_shutdown() {
  echo "Cleaning up now"
  kind delete cluster --name $CLUSTER_NAME 2>/dev/null
  exit 0
}

trap _shutdown SIGTERM
trap _shutdown SIGINT

# Create the cluster and setup configuration
kind create cluster --name $CLUSTER_NAME --config kind-config.yml
kubectl config use-context kind-$CLUSTER_NAME
API_PORT=$(kubectl config view --minify | grep server | cut -f 2- -d ":" | tr -d " " | cut -f3 -d ":")
echo "Updating kubectl config to use server https://${kubeApiServerHost}:${API_PORT}"
kubectl config set-cluster kind-$CLUSTER_NAME --insecure-skip-tls-verify=true --server=https://${kubeApiServerHost}:${API_PORT}

# Load the images into the cluster since some are only local and others take a while to download
#docker pull $SYNCER_IMAGE
docker tag $SYNCER_IMAGE flux-syncer:to-use
kind load docker-image flux-syncer:to-use --name $CLUSTER_NAME

#docker pull $SYNCER_TESTS_IMAGE
docker tag $SYNCER_TESTS_IMAGE flux-syncer-tests:to-use
kind load docker-image flux-syncer-tests:to-use --name $CLUSTER_NAME

docker pull mockserver/mockserver:$mockserverImageTag
docker tag mockserver/mockserver:$mockserverImageTag mockserver/mockserver:to-use
kind load docker-image mockserver/mockserver:to-use --name $CLUSTER_NAME

# Deploy Flux CRDs and system
echo "\nDeploying Flux CRDs and components"
kubectl apply -f ./flux.yaml

# Start the actual tests and wait for them to complete
echo "\nStarting tests now..."
kubectl apply -f ./test-pod.yaml

echo -n "Waiting for test container to start.."
while [[ $(kubectl get pod flux-key-syncer-tests -o 'jsonpath={.status.phase}') != "Running" ]]; do
    echo -n "."
    sleep 2
done

kubectl get pods

kubectl logs -f flux-key-syncer-tests
exitCode=$(kubectl get pod flux-key-syncer-tests -o 'jsonpath={.status.containerStatuses[0].state.terminated.exitCode}')

kind delete cluster --name $CLUSTER_NAME 2>/dev/null

if [ "$exitCode" != "0" ]; then
  exit $exitCode
fi
